import mongoengine


def global_init(connection_str: str, db_name: str):
    mongoengine.register_connection(alias='core', host=connection_str, name=db_name)


# Main Class Call
if __name__ == "__main__":
    pass
