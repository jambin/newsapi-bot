import datetime
import mongoengine


class NewsSource(mongoengine.Document):
    created_at = mongoengine.DateTimeField(defualt=datetime.datetime.now)
    source_id = mongoengine.StringField(required=True)
    name = mongoengine.StringField(required=True)
    meta = {
        'db_alias': 'core',
        'collection': 'news_source',
        'indexed': [
            'source_id',
            'name'
        ],
        'ordering': ['-source_id'],
    }
    pass
