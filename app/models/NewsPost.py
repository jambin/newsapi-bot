import datetime
import mongoengine


class NewsPost(mongoengine.Document):
    published_at = mongoengine.DateTimeField(required=True)
    source_id = mongoengine.ObjectIdField(required=True)
    author = mongoengine.StringField()
    title = mongoengine.StringField()
    description = mongoengine.StringField()
    url = mongoengine.StringField()
    url_to_img = mongoengine.StringField()
    content = mongoengine.StringField()
    tag = mongoengine.StringField()
    meta = {
        'db_alias': 'core',
        'collection': 'news_post',
        'indexed': [
            'title',
            'author',
            'source_id',
            'tag'
        ],
        'ordering': ['-title'],
    }
    pass
