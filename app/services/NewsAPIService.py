from datetime import date, datetime

from models.NewsPost import NewsPost
from models.NewsSource import NewsSource


class NewsAPIService:
    @classmethod
    def find_source(cls, source):
        by_id = NewsSource.objects(source_id=source['id']).first()
        if by_id is None:
            by_name = NewsSource.objects(source_id=source['name']).first()
            return by_name
        else:
            return by_id

    @classmethod
    def add_source(cls, source):
        s = NewsSource()
        if source['id'] is None:
            if source['name'] is None:
                return None
            else:
                s.source_id = source['name']
        else:
            s.source_id = source['id']
        s.name = source['name']
        s.save()
        return s

    @classmethod
    def add_news(cls, news, source: NewsSource, tag: str):
        p = NewsPost()

        p.source_id = source.id
        p.title = news['title']
        p.author = news['author']
        p.description = news['description']
        p.url = news['url']
        p.url_to_img = news['urlToImage']
        p.published_at = datetime.strptime(news['publishedAt'][0:10] + " " + news['publishedAt'][11:-1],
                                           "%Y-%m-%d %H:%M:%S")
        p.tag = tag
        p.content = news['content']
        p.save()
        return p

    @classmethod
    def find_news(cls, title: str):
        return NewsPost.objects(title=title).first()
