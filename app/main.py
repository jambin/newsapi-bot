from db import global_init
import configparser
import logging
from newsapi import NewsApiClient
from flask_api import FlaskAPI, status

app = FlaskAPI(__name__)
app.config.update(
    DEBUG=False,
    TESTING=False)

# ------------------------------------------------------------------------------------------------------
# Configs
# Logging
from services.NewsAPIService import NewsAPIService

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Configuring bot
try:
    config = configparser.ConfigParser()
    config.read_file(open('config.ini'))
    newsapi = NewsApiClient(api_key=config['DEFAULT']['APP_KEY'])

except IOError:
    logger.error("Cant Open The Config File !")
    exit(1)


def main():
    logger.info("Bot Starts !")
    try:
        global_init(config['DEFAULT']['DB'], 'news_api')
        logger.info("DB Inited !")
    except:
        logger.error("DB Init FAIL !")
        exit(1)

    app.run(debug=False)

    for tag in config['DEFAULT']['TAGS'].split(','):
        logger.info(f"Get Data For Tag => '{tag}'")
        page = 0
        total_page_size = 1
        while page <= total_page_size:
            page += 1
            response = None
            logger.info(f"Download Page => '{page}'")
            try:
                response = newsapi.get_everything(q=tag,
                                                  page=page,
                                                  page_size=100)
            except Exception as e:
                logger.error("can't get data from API")
                break

            if response['status'] == 'ok':
                total_page_size = int(int(response['totalResults']) / 100)
                for post in response['articles']:
                    source = NewsAPIService.find_source(post['source'])
                    if source is None:
                        source = NewsAPIService.add_source(post['source'])
                        if source is None:
                            continue
                    if NewsAPIService.find_news(post['title']) is None:
                        p = NewsAPIService.add_news(post, source, tag)
                        logger.info(str(p.id) + " is added.")
            else:
                logger.error('exit with non 200 status code!')
                break


# Main Class Call
if __name__ == "__main__":
    main()
