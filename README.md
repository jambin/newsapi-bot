# NewsAPI Bot

This bot will collect data from [NewsApi](https://newsapi.org/) web and create a simple api to work
with.

## Usage

1. Get your APIKey from [NewsApi](https://newsapi.org/) place it 
    on -> app/config.ini
2. Add Your mongo to app/config.ini
3. Build -> `docker build -t news_bot .`
4. Run -> `docker run -d --name news_bot -p 5000:5002 news_bot`
5. Simple Usage -> http://localhost:5001/weather/tehran

## Maintainers

- Armin Sadreddin
- Alireza Sadraii